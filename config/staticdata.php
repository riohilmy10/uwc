<?php 

return [
    'working_hours_per_shift' => 12,
    'units_per_batch' => 100,
    'defects' => [
        'table-header' => [
            '0' => 'pfr_no',
            '1' => 'production_line_no',
            '2' => 'station_no',
            '3' => 'object_type',
            '4' => 'traceability_no',
            '5' => 'report_datetime',
            '6' => 'employee_id',
            '7' => 'reason',
        ]
    ],
    'station1' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'production_line_no',
            '2' => 'body_traceability_no',
            '3' => 'pfr_1_no',
            '4' => 'batch_start_datetime',
            '5' => 'batch_end_datetime',
            '6' => 'incomplete',
            '7' => 'reason_of_incompletion',
        ]
    ],
    'station2' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'production_line_no',
            '2' => 'cap_traceability_no',
            '3' => 'pfr_1_no',
            '4' => 'pfr_2_no',
            '5' => 'batch_start_datetime',
            '6' => 'batch_end_datetime',
            '7' => 'incomplete',
            '8' => 'reason_of_incompletion',
        ]
    ],
    'station3' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'production_line_no',
            '2' => 'pfr_1_no',
            '3' => 'pfr_2_no',
            '4' => 'pfr_3_no',
            '5' => 'batch_start_datetime',
            '6' => 'batch_end_datetime',
            '7' => 'incomplete',
            '8' => 'reason_of_incompletion',
        ]
    ],
    'employee_analytics' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'employee_name',
            '2' => 'total_working_hours',
            '3' => 'total_inactive_hours',
            '4' => 'total_productive_hours',
        ]
    ],
    'employee_shifts' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'pfr_no',
            '2' => 'station_no',
            '3' => 'clock_in_datetime',
            '4' => 'clock_out_datetime',
            '5' => 'working_hours',
            '6' => 'inactive_hours',
        ]
    ],
    'employee_limit' => 3,
    'production_line_limit' => 1
];