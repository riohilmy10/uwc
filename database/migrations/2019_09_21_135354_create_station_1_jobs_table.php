<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStation1JobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station_1_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_id')->index();
            $table->unsignedTinyInteger('production_line_no');
            $table->string('body_traceability_no');
            $table->string('pfr_1_no');
            $table->datetime('batch_start_datetime');
            $table->datetime('batch_end_datetime');
            $table->integer('incomplete')->unsigned()->default(0);
            $table->text('reason_of_incompletion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('station_1_jobs');
    }
}
