<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStation2JobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station_2_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_id')->index();
            $table->unsignedTinyInteger('production_line_no');
            $table->string('cap_traceability_no');
            $table->string('pfr_1_no');
            $table->string('pfr_2_no');
            $table->datetime('batch_start_datetime');
            $table->datetime('batch_end_datetime');
            $table->unsignedSmallInteger('incomplete')->default(0);
            $table->text('reason_of_incompletion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('station_2_jobs');
    }
}
