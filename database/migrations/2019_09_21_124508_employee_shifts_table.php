<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_shifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_id')->index();
            $table->datetime('clock_in_datetime');
            $table->unsignedTinyInteger('production_line_no');
            $table->unsignedTinyInteger('station_no');
            $table->unsignedSmallInteger('working_hours')->default(0);
            $table->unsignedSmallInteger('inactive_hours')->default(0);
            $table->datetime('clock_out_datetime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_shifts');        
    }
}
