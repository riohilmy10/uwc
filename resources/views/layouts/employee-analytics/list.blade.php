@extends('master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Employee Analytics</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Employee Analytics</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header text-white bg-success">
                            <h3 class="card-title">Employee Analytics Table</h3>
                        </div>
                        <div class="card-body">
                            <input type="hidden" id="datatable_api_route" value="{{ route('api.employee-analytics.datatable.list') }}">
                            <table id="employee_analytics_table" class="table table-bordered table-hover w-100">
                                <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>Employee Name</th>
                                        <th>Total Working Hours</th>
                                        <th>Total Inactive Hours</th>
                                        <th>Total Productive Hours</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#employee_analytics_table').DataTable({
            "lengthChange": false,
            "searching": false,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "autoWidth": true,
            "ajax": $("#datatable_api_route").val(),
            "columns": [
                { data: 'employee_id' },
                { data: 'name' },
                { data: 'total_working_hours' },
                { data: 'total_inactive_hours' },
                { data: 'total_productive_hours' },
            ]
        });
    })
</script>
@endsection