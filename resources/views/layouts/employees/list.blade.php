@extends('master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Employees</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Employee Management</li>
                    </ol>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-6 col-md-3">
                    <a id="add_new_employee" href="{{ route('employees.view.create') }}" class="btn btn-success">
                        <i class="fas fa-plus pr-2"></i>
                        Add New Employee
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header text-white bg-success">
                            <h3 class="card-title">Employees Table</h3>
                        </div>
                        <div class="card-body">
                            <table id="employee_table" class="table table-bordered table-hover w-100">
                                <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>Full Name</th>
                                        <th>Contact Number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($employee_list as $employee)
                                        <tr>
                                            <td>
                                                <a href="{{ route('employees.view.edit', ['id' => $employee['id']]) }}">
                                                    {{ $employee['employee_id'] }}
                                                </a>
                                            </td>
                                            <td>{{ $employee['name'] }}</td>
                                            <td>{{ $employee['contact_no'] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="employee_count" value="{{ count($employee_list) }}">
<input type="hidden" id="employee_max" value="{{ config('staticdata.employee_limit') }}">

<script>
$(function() {
    $('#employee_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
    });
})

$(document).on('click', '#add_new_employee', function () {
    const employee_count = $("employee_count").val();
    const employee_limit = $("employee_limit").val();

    console.log(employee_count);
    console.log(employee_limit);
    
    if (employee_count == employee_limit) {
        alert('You have reached maximum number of employees, please contact our company\'s representative to add more employee');
        return false;
    }

});
</script>
@endsection