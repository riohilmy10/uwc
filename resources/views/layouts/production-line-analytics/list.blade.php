@extends('master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Production Line Analytics</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Production Line Analytics</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    
    <!-- /.content-header -->
    <div class="content">
        <div class="container-fluid">
            <section class="col-lg-12">
                <div class="row">
                    <div class="col-md-2 col-4">
                        <div class="form-group">
                            <label for="production_line_dropdown">Production Line:</label>
                            <select class="form-control select2 select2-success" data-dropdown-css-class="select2-success"
                                name="production_line" id="production_line_dropdown">
                                @for ($i = 1; $i <= config('staticdata.production_line_limit'); $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                                <option value="2">2</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- row -->
                <div class="row pt-1 pb-1">
                    <div class="col-12">
                        <!-- jQuery Knob -->
                        <div class="card">
                            <div class="card-header text-white bg-success">
                                <h3 class="card-title">
                                    <i class="fas fa-tachometer-alt pr-2"></i> 
                                    Daily Average Reports
                                </h3>
                            </div>
                            <div class="card-body">
                                <div id="spinner1" class="text-center">
                                    <img src="/img/loading-spinner.gif" alt="loading.." style="height: 100%">
                                </div>
                                <div id="daily_percentage" class="row" style="display: none;">
                                    <input type="hidden" id="daily_percentage_chart_url" value="{{ route('api.charts.daily-percentage') }}">
                                    <div class="col-6 col-md-3 text-center">
                                        <i class="fas fa-clock fa-2x pb-3 daily-average-icon"></i><br>
                                        <input id="working_hours" type="text" class="knob" value="" 
                                            data-width="120" data-height="120" data-fgColor="#28a745"
                                            data-min="0" data-max="12" data-step="0.01" data-readOnly=true>

                                        <div class="knob-label">Daily average working hours</div>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <i class="fas fa-business-time fa-2x pb-3 daily-average-icon"></i><br>
                                        <input id="productive_hours" type="text" class="knob" value="" 
                                            data-width="120" data-height="120" data-fgColor="#fbff29"
                                            data-min="0" data-max="12" data-step="0.01" data-readOnly=true>

                                        <div class="knob-label">Daily average productive hours</div>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <i class="fas fa-tasks fa-2x pb-3 daily-average-icon"></i><br>
                                        <input id="batch_completed" type="text" class="knob" value="" data-width="120"
                                            data-height="120" data-fgColor="brown"
                                            data-min="0" data-max="200" data-step="0.01" data-readOnly=true>

                                        <div class="knob-label">Daily average batch completed</div>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <i class="fas fa-dumpster fa-2x pb-3 daily-average-icon"></i><br>
                                        <input id="defects" type="text" class="knob" value=""
                                            data-width="120" data-height="120" data-fgColor="red"
                                            data-min="0" data-max="200" data-step="0.01" data-readOnly=true>

                                        <div class="knob-label">Daily average defective units</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-white bg-success">
                                <h3 class="card-title">
                                    <i class="far fa-chart-bar pr-2"></i>
                                    Batch Completion Chart
                                </h3>
                            </div>
                            <div class="card-body">
                                <input type="hidden" id="batch_completed_chart_url" value="{{ route('api.charts.batch-completed') }}">
                                <div id="spinner2" class="text-center">
                                    <img src="/img/loading-spinner.gif" alt="loading..">
                                </div>
                                <div id="batch_completed_chart_section" class="row" style="display: none;">
                                    <div class="w-100" id="batch_completed_chart">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-white bg-success">
                                <h3 class="card-title">
                                    <i class="far fa-chart-bar pr-2"></i>
                                    Defective Units Chart
                                </h3>
                            </div>
                            <div class="card-body">
                                <input type="hidden" id="defects_chart_url" value="{{ route('api.charts.defects') }}">
                                <div id="spinner3" class="text-center">
                                    <img src="/img/loading-spinner.gif" alt="loading..">
                                </div>
                                <div id="defects_chart_section" class="row" style="display: none;">
                                    <div class="w-100" id="defects_chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content-wrapper -->

<form id="daily_percentage_chart_form" method="get"></form>
<form id="batch_completed_chart_form" method="get"></form>
<form id="defects_chart_form" method="get"></form>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<!-- <script src="https://code.highcharts.com/modules/accessibility.js"></script> -->

<script>
$(function () {
    $.widget.bridge('uibutton', $.ui.button)
    
    $(".knob").knob({
        'dynamicDraw': true,
        'format':function(value){
            return value;
        }
    });

    $('.select2').select2()
    $("#daily_percentage_chart_form").submit();
    $("#batch_completed_chart_form").submit();
    $("#defects_chart_form").submit();
})

$(document).on('change', '#production_line_dropdown', function(e) {
    $("#daily_percentage").hide();
    $("#batch_completed_chart_section").hide();
    $("#defects_chart_section").hide();
    $("#spinner1").show();
    $("#spinner2").show();
    $("#spinner3").show();
    $("#daily_percentage_chart_form").submit();
    $("#batch_completed_chart_form").submit();
    $("#defects_chart_form").submit();
});

$(document).on('submit', '#daily_percentage_chart_form', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    
    $.ajax(
    {
        url: $("#daily_percentage_chart_url").val() + '?production_line=' + $("#production_line_dropdown").val(),
        type: "GET",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success:function(output){
            $("#spinner1").hide();
            $("#daily_percentage").show();
            var chart = initDailyPercentageChart(output.attributes);
        }						
    });
});

$(document).on('submit', '#batch_completed_chart_form', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    
    $.ajax(
    {
        url: $("#batch_completed_chart_url").val() + '?production_line=' + $("#production_line_dropdown").val(),
        type: "GET",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function(output){
            $("#spinner2").hide();
            $("#batch_completed_chart_section").show();
            var chart = initBatchCompletedChart(output.attributes);
        }						
    });
});

$(document).on('submit', '#defects_chart_form', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    
    $.ajax(
    {
        url: $("#defects_chart_url").val() + '?production_line=' + $("#production_line_dropdown").val(),
        type: "GET",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success:function(output){
            $("#spinner3").hide();
            $("#defects_chart_section").show();
            var chart = initDefectsChart(output.attributes);
        }						
    });
});

function initDailyPercentageChart(response)
{
    $("#working_hours").val(response['working_hours']);
    $("#productive_hours").val(response['productive_hours']);
    $("#batch_completed").val(response['batch_completed']);
    $("#defects").val(response['defects']);
    
    $(".knob").change();
}

function initBatchCompletedChart(response)
{
    var batch_completed_chart = Highcharts.chart('batch_completed_chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Batch Completion in ' + response['year']
        },
        subtitle: {
            text: 'Number of batch completed each month in current year'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Batch Completed'
            },
            align: 'middle'
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}' + response['year'] + '</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} batch(es)</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
        },
        series: [{
            name: 'Station 1',
            data: (function () {
                var data = [];
                $.each(response['station1'], function (i, item) {
                    data.push(item);
                });

                return data;
            }()),
        }, {
            name: 'Station 2',
            data: (function () {
                var data = [];
                $.each(response['station2'], function (i, item) {
                    data.push(item);
                });

                return data;
            }()),
        }, {
            name: 'Station 3',
            data: (function () {
                var data = [];
                $.each(response['station3'], function (i, item) {
                    data.push(item);
                });

                return data;
            }()),
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    }, function(chart) {
        var arr = chart.options.exporting.buttons.contextButton.menuItems;
        var index = arr.indexOf("viewData");
        if (index !== -1) arr.splice(index, 1);
        var index = arr.indexOf("openInCloud");
        if (index !== -1) arr.splice(index, 1);
        var index = arr.indexOf("separator");
        if (index !== -1) arr.splice(index, 1);
        var index = arr.indexOf("separator");
        if (index !== -1) arr.splice(index, 1);
    });
}

function initDefectsChart(response)
{
    var series_data = [];
    $.each(response['defects'], function (i, item1) {
        series_data.push({
            name: i.charAt(0).toUpperCase() + i.substr(1).toLowerCase(),
            data: (function () {
                var data = [];
                $.each(response['defects'][i], function (j, item2) {
                    data.push(item2);
                });

                return data;
            }()),
        });
    });

    var defects_chart = Highcharts.chart('defects_chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Defective Units in ' + response['year']
        },
        subtitle: {
            text: 'Number of defective units each month in current year'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of defective units'
            },
            align: 'middle'
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}' + response['year'] + '</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} unit(s)</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
        },
        series: series_data,
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    });
}
</script> 
@endsection