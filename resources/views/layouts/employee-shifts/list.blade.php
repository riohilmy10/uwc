@extends('master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Employee Shifts</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Employee Shifts</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-4">
                    <div class="form-group">
                        <label for="production_line_dropdown">Production Line:</label>
                        <select class="form-control select2 select2-success" data-dropdown-css-class="select2-success"
                            name="production_line" id="production_line">
                            @for ($i = 1; $i <= config('staticdata.production_line_limit'); $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                            <option value="2">2</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header text-white bg-success">
                            <h3 class="card-title">Employee Shifts Table</h3>
                        </div>
                        <div class="card-body">
                            <input type="hidden" id="employee_shift_datatable_api_route" value="{{ route('api.employee-shifts.datatable.list') }}">
                            <table id="employee_shifts_table" class="table table-bordered table-hover w-100">
                                <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>PFR No</th>
                                        <th>Station</th>
                                        <th>Shift Started At</th>
                                        <th>Shift Ended At</th>
                                        <th>Working Hours</th>
                                        <th>Inactive Hours</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var employee_shifts_datatable;
var production_line = 1;

$(function() {
    employee_shifts_datatable = $('#employee_shifts_table').DataTable({
        "lengthChange": false,
        "searching": false,
        "pageLength": 10,
        "processing": true,
        "serverSide": true,
        "autoWidth": true,
        "order": [[ 3, "desc" ]],
        "ajax": {
            "url": $("#employee_shift_datatable_api_route").val(),
            "data": function ( d ) {
                d.production_line_no = $("#production_line").val()
            }
        },
        "columns": [
            { data: 'employee_id' },
            { data: 'pfr_no' },
            { data: 'station_no' },
            { data: 'clock_in_datetime' },
            { data: 'clock_out_datetime' },
            { data: 'working_hours' },
            { data: 'inactive_hours' }
        ]
    });
})

$(document).on('change', '#production_line', function(e) {
    // production_line = $(this).val();
    // employee_shifts_datatable.clear();
    employee_shifts_datatable.draw();
});
</script>
@endsection