@extends('master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Station Jobs</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Station Jobs</li>
                    </ol>
                </div>
            </div>
            <!-- <div class="row mb-2">
                <div class="col-3">
                    <a href="{{ route('defects.view.create') }}" class="btn btn-success">
                        <i class="fas fa-plus pr-2"></i>
                        Add New Defective Unit
                    </a>
                </div>
            </div> -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs">
                        <li class="nav-item tab-1">
                            <a class="nav-link active" data-toggle="tab" href="#station1">Station 1</a>
                        </li>
                        <li class="nav-item tab-2">
                            <a class="nav-link" data-toggle="tab" href="#station2">Station 2</a>
                        </li>
                        <li class="nav-item tab-3">
                            <a class="nav-link" data-toggle="tab" href="#station3">Station 3</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="station1">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-success station">
                                <div class="card-body">
                                    <input type="hidden" id="station1_datatable_api_route" value="{{ route('api.station-1-jobs.datatable.list') }}">
                                    <table id="station1_table" class="table table-bordered table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th>Employee ID</th>
                                                <th>Production Line</th>
                                                <th>Body Traceability</th>
                                                <th>PFR 1</th>
                                                <th>Batch Started At</th>
                                                <th>Batch Ended At</th>
                                                <th>Incomplete Unit</th>
                                                <th>Reason of Incompletion</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="station2">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-success station">
                                <div class="card-body">
                                    <input type="hidden" id="station2_datatable_api_route" value="{{ route('api.station-2-jobs.datatable.list') }}">
                                    <table id="station2_table" class="table table-bordered table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th>Employee ID</th>
                                                <th>Production Line</th>
                                                <th>Cap Traceability</th>
                                                <th>PFR 1</th>
                                                <th>PFR 2</th>
                                                <th>Batch Started At</th>
                                                <th>Batch Ended At</th>
                                                <th>Incomplete Unit</th>
                                                <th>Reason of Incompletion</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="station3">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-success station">
                                <div class="card-body">
                                    <input type="hidden" id="station3_datatable_api_route" value="{{ route('api.station-3-jobs.datatable.list') }}">
                                    <table id="station3_table" class="table table-bordered table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th>Employee ID</th>
                                                <th>Production Line</th>
                                                <th>PFR 1</th>
                                                <th>PFR 2</th>
                                                <th>PFR 3</th>
                                                <th>Batch Started At</th>
                                                <th>Batch Ended At</th>
                                                <th>Incomplete Unit</th>
                                                <th>Reason of Incompletion</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#station1_table').DataTable({
            "lengthChange": false,
            "searching": false,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "autoWidth": true,
            "order": [[ 4, "desc" ]],
            "ajax": $("#station1_datatable_api_route").val(),
            "columns": [
                { data: 'employee_id' },
                { data: 'production_line_no' },
                { data: 'body_traceability_no' },
                { data: 'pfr_1_no' },
                { data: 'batch_start_datetime' },
                { data: 'batch_end_datetime' },
                { data: 'incomplete' },
                { data: 'reason_of_incompletion' }
            ]
        });
        
        $('#station2_table').DataTable({
            "lengthChange": false,
            "searching": false,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "autoWidth": true,
            "order": [[ 5, "desc" ]],
            "ajax": $("#station2_datatable_api_route").val(),
            "columns": [
                { data: 'employee_id' },
                { data: 'production_line_no' },
                { data: 'cap_traceability_no' },
                { data: 'pfr_1_no' },
                { data: 'pfr_2_no' },
                { data: 'batch_start_datetime' },
                { data: 'batch_end_datetime' },
                { data: 'incomplete' },
                { data: 'reason_of_incompletion' }
            ]
        });
        
        $('#station3_table').DataTable({
            "lengthChange": false,
            "searching": false,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "autoWidth": true,
            "order": [[ 5, "desc" ]],
            "ajax": $("#station3_datatable_api_route").val(),
            "columns": [
                { data: 'employee_id' },
                { data: 'production_line_no' },
                { data: 'pfr_1_no' },
                { data: 'pfr_2_no' },
                { data: 'pfr_3_no' },
                { data: 'batch_start_datetime' },
                { data: 'batch_end_datetime' },
                { data: 'incomplete' },
                { data: 'reason_of_incompletion' }
            ]
        });
    })
</script>
@endsection