@extends('master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Defects</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Defects</li>
                    </ol>
                </div>
            </div>
            <!-- <div class="row mb-2">
                <div class="col-3">
                    <a href="{{ route('defects.view.create') }}" class="btn btn-success">
                        <i class="fas fa-plus pr-2"></i>
                        Add New Defective Unit
                    </a>
                </div>
            </div> -->
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header text-white bg-success">
                            <h3 class="card-title">Defects Table</h3>
                        </div>
                        <div class="card-body">
                            <input type="hidden" id="datatable_api_route" value="{{ route('api.defects.datatable.list') }}">
                            <table id="defect_table" class="table table-bordered table-hover w-100">
                                <thead>
                                    <tr>
                                        <th>PFR</th>
                                        <th>Production Line</th>
                                        <th>Station</th>
                                        <th>Type</th>
                                        <th>Traceability</th>
                                        <th>Reported At</th>
                                        <th>Reported By</th>
                                        <th>Reason</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#defect_table').DataTable({
            "lengthChange": false,
            "searching": false,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "order": [[ 5, "desc" ]],
            "ajax": $("#datatable_api_route").val(),
            "columns": [
                { data: 'pfr_no' },
                { data: 'production_line_no' },
                { data: 'station_no' },
                { data: 'object_type' },
                { data: 'traceability_no' },
                { data: 'report_datetime' },
                { data: 'employee_id' },
                { data: 'reason' }
            ]
        });
    })
</script>
@endsection