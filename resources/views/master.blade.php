<!DOCTYPE html>
<html lang="en" class="gr__adminlte_io">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>UWC</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- <script src="{{ asset('/node_modules/admin-lte/plugins/jquery-ui/jquery-ui.min.js') }}"></script> -->
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('header')

        @include('sidebar')

        <section class="content">
            @yield('content')
        </section>

        @include('footer')
    </div>

</body>
</html>
