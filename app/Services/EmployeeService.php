<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\EmployeeShift;

class EmployeeService
{
    public function getTotalWorkingHours($employee_id)
    {
        $sum = EmployeeShift::where('employee_id', $employee_id)
            ->where('clock_out_datetime', '!=', 'NULL')
            ->sum('working_hours');
        
        return $sum;
    }
    
    public function getTotalInactiveHours($employee_id)
    {
        $sum = EmployeeShift::where('employee_id', $employee_id)
            ->where('clock_out_datetime', '!=', 'NULL')
            ->sum('inactive_hours');

        return $sum;
    }
    
    public function getTotalProductiveHours($employee_id)
    {
        $total_working_hours = $this->getTotalWorkingHours($employee_id);
        $total_inactive_hours = $this->getTotalInactiveHours($employee_id);
        
        $productive_hours = $total_working_hours - $total_inactive_hours;

        if ($productive_hours <= 0) {
            $productive_hours = 0;
        }

        return $productive_hours;
    }

    public function formatInHours($value)
    {
        // Working hours in minutes, divided by 60 to make hours
        $total = $value / 60;

        return number_format($total, 2, '.', ',');
    }
}