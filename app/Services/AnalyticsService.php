<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\EmployeeShift;
use App\Models\Defect;
use App\Models\Station1Job;
use App\Models\Station2Job;
use App\Models\Station3Job;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AnalyticsService
{
    public function getDailyPercentage($production_line = null)
    {
        return [
            'working_hours' => $this->getDailyAverageWorkingHoursPercentage($production_line),
            'productive_hours' => $this->getDailyAverageProductiveHoursPercentage($production_line),
            'batch_completed' => $this->getDailyAverageBatchCompleted($production_line),
            'defects' => $this->getDailyAverageDefectiveUnits($production_line),
        ];
    }

    public function getDailyAverageWorkingHoursPercentage($production_line = null)
    {
        $sum = EmployeeShift::where('clock_out_datetime', '!=', 'NULL')
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->sum('working_hours');

        $count = EmployeeShift::where('clock_out_datetime', '!=', 'NULL')
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )->count();

        if ($sum > 0 && $count > 0) {
            // $divisor = $count * config('staticdata.working_hours_per_shift') * 60;
            $total = $sum / 60 / $count;
        } else {
            $total = 0;
        }

        if ($total > 12) {
            $total = 12;
        }

        return number_format($total, 2, '.', ',');
    }

    public function getDailyAverageProductiveHoursPercentage($production_line = null)
    {
        $data = EmployeeShift::select(
                DB::raw('SUM(working_hours) as total_working_hours'),
                DB::raw('SUM(inactive_hours) as total_inactive_hours')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('clock_out_datetime', '!=', 'NULL')->first();

        $count = EmployeeShift::where('clock_out_datetime', '!=', 'NULL')->count();

        !$data['total_working_hours'] ? $data['total_working_hours'] = 0 : null;
        !$data['total_inactive_hours'] ? $data['total_inactive_hours'] = 0 : null;
        
        $productive_hours = $data['total_working_hours'] - $data['total_inactive_hours'];

        if ($productive_hours > 0) {
            // $divisor = $count * config('staticdata.working_hours_per_shift') * 60;
            $total = $productive_hours / 60 / $count;
        } else {
            $total = 0;
        }

        if ($total > 12) {
            $total = 12;
        }

        return number_format($total, 2, '.', ',');
    }

    public function getDailyAverageBatchCompleted($production_line = null)
    {
        $station1 = Station1Job::
            when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('incomplete', 0)
            ->count();

        $station2 = Station2Job::
            when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('incomplete', 0)
            ->count();
            
        $station3 = Station3Job::
            when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('incomplete', 0)
            ->count();
        
        // Get first date of the work
        $station1_record = Station1Job::select('batch_start_datetime')
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->orderBy('batch_start_datetime', 'asc')
            ->first();

        // Get last date of the work
        $station1_record2 = Station1Job::select('batch_start_datetime')
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->orderBy('batch_start_datetime', 'desc')
            ->first();

        if ($station1_record) {
            $start_date = Carbon::parse($station1_record->batch_start_datetime);
            $last_date = Carbon::parse($station1_record2->batch_start_datetime);
            $total_days = $last_date->diffInDays($start_date) + 1;
        } else {
            $total_days = 1;
        }

        $total = ($station1 + $station2 + $station3) / $total_days;

        return number_format($total, 2, '.', ',');
    }
    
    public function getDailyAverageDefectiveUnits($production_line = null)
    {
        $defects = Defect::
            when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->count();
        
        // Get first date of the work
        $station1_record = Station1Job::select('batch_start_datetime')
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->orderBy('batch_start_datetime', 'asc')
            ->first();
            
        // Get last date of the work
        $station1_record2 = Station1Job::select('batch_start_datetime')
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->orderBy('batch_start_datetime', 'desc')
            ->first();

        if ($station1_record) {
            $start_date = Carbon::parse($station1_record->batch_start_datetime);
            $last_date = Carbon::parse($station1_record2->batch_start_datetime);
            $total_days = $last_date->diffInDays($start_date) + 1;
        } else {
            $total_days = 1;
        }

        $total = $defects / $total_days;

        return number_format($total, 2, '.', ',');
    }
    
    public function getBatchCompleted($production_line = null)
    {
        $year = Carbon::now()->year;

        $station1 = Station1Job::select(
                DB::raw('COUNT(id) as batch'),
                DB::raw('MONTH(batch_start_datetime) month')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('incomplete', 0)
            ->whereYear('batch_start_datetime', $year)
            ->groupBy('month')
            ->get();
        
        $station2 = Station2Job::select(
                DB::raw('COUNT(id) as batch'),
                DB::raw('MONTH(batch_start_datetime) month')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('incomplete', 0)
            ->whereYear('batch_start_datetime', $year)
            ->groupBy('month')
            ->get();
        
        $station3 = Station3Job::select(
                DB::raw('COUNT(id) as batch'),
                DB::raw('MONTH(batch_start_datetime) month')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->whereYear('batch_start_datetime', $year)
            ->groupBy('month')
            ->get();
        
        if (!$station1->isEmpty()) {
            foreach ($station1 as $job) {
                $station1_month[$job->month] = $job->batch;
            }
        } else {
            $station1_month = [];
        }

        if (count($station1_month) < 12) {
            for ($i = 1; $i <= 12; $i++) {
                $station1_ordered[] = $station1_month[$i] ?? 0;
            }
        }
        
        if (!$station2->isEmpty()) {
            foreach ($station2 as $job) {
                $station2_month[$job->month] = $job->batch;
            }
        } else {
            $station2_month = [];
        }

        if (count($station2_month) < 12) {
            for ($i = 1; $i <= 12; $i++) {
                $station2_ordered[] = $station2_month[$i] ?? 0;
            }
        }
        
        if (!$station3->isEmpty()) {
            foreach ($station3 as $job) {
                $station3_month[$job->month] = $job->batch;
            }
        } else {
            $station3_month = [];
        }

        if (count($station3_month) < 12) {
            for ($i = 1; $i <= 12; $i++) {
                $station3_ordered[] = $station3_month[$i] ?? 0;
            }
        }

        return [
            'year' => $year,
            'station1_ordered' => $station1_ordered,
            'station2_ordered' => $station2_ordered,
            'station3_ordered' => $station3_ordered,
        ];
    }

    public function getDefectiveUnits($production_line = null)
    {
        $year = Carbon::now()->year;

        $defects = Defect::select(
                'object_type',
                DB::raw('MONTH(report_datetime) month'),
                DB::raw('COUNT(id) as defect')
            )
            ->whereYear('report_datetime', $year)
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->groupBy('object_type','month')
            ->get();

        if (!$defects->isEmpty()) {
            foreach ($defects as $defect) {
                $defects_restructured[$defect->object_type][$defect->month] = $defect->defect;
            }
        } else {
            $defects = Defect::select('object_type')->groupBy('object_type')->get();
            $i = 1;
            foreach ($defects as $defect) {
                $defects_restructured[$defect->object_type][$i] = 0;
                $i++;
            }
        }

        foreach ($defects_restructured as $object_type => $value) {
            for ($i = 1; $i <= 12; $i++) {
                $defects_restructured[$object_type][$i] = $defects_restructured[$object_type][$i] ?? 0;
            }
        }
        
        return [
            'year' => $year,
            'defects_ordered' => $defects_restructured
        ];
    }
}
