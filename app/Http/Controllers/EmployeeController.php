<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Validator;

class EmployeeController extends Controller
{
    public function list(Request $request)
    {
        session(['active_nav' => 'employees']);

        $employee_list = Employee::where('is_active', true)->get();

        return view('layouts.employees.list', compact('employee_list'));
    }

    public function createView(Request $request)
    {
        session(['active_nav' => 'employees']);

        return view('layouts.employees.create');
    }

    public function create(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'employee_id' => 'required|max:255',
                'name'        => 'required|between:2,100',
                'contact_no'  => 'required|max:20'
            ]
        );

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        
        Employee::create(
            [
                'employee_id' => $request->employee_id,
                'name'        => $request->name,
                'contact_no'  => $request->contact_no,
                'is_active'   => true,
            ]
        );

        return redirect()->route('employees.view.list');
    }

    public function edit(Request $request, $id)
    {
        session(['active_nav' => 'employees']);
        
        $employee = Employee::find($id);

        return view('layouts.employees.edit')->with('employee', $employee);
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'employee_id' => 'required|max:255',
                'name'        => 'required|between:2,100',
                'contact_no'  => 'required|max:20'
            ]
        );

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        Employee::find($id)->update(
            [
                'employee_id' => $request->employee_id,
                'name'        => $request->name,
                'contact_no'  => $request->contact_no
            ]
        );

        return redirect()->route('employees.view.list');
    }
}
