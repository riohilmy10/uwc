<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Station2Job;
use Carbon\Carbon;

class Station2JobApiController extends Controller
{
    public function create(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        try {
            Station2Job::create(
                [
                    'employee_id'            => $data['employee_id'],
                    'production_line_no'     => $data['production_line_no'],
                    'cap_traceability_no'    => $data['cap_traceability_no'],
                    'pfr_1_no'               => $data['pfr_1_no'],
                    'pfr_2_no'               => $data['pfr_2_no'],
                    'batch_start_datetime'   => $data['batch_start_datetime'],
                    'batch_end_datetime'     => $data['batch_end_datetime'],
                    'incomplete'             => $data['incomplete'],
                    'reason_of_incompletion' => $data['reason_of_incompletion'],
                ]
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['message' => $e->getMessage()], 422);
        }

        return response()->json(['message' => 'Station 2 job record has been created.'], 200);
    }
    
    public function datatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        if ($draw == 1) {
            $sort = 'batch_start_datetime';
            $order = 'desc';
        } else {
            $sort = config('staticdata.station2.table-header.' . $request['order'][0]['column']);
            $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';
        }

        $search = $request->filter ?? false;

        $total = Station2Job::count();
        $job_list = Station2Job::select(
            'employee_id',
            'production_line_no',
            'cap_traceability_no',
            'pfr_1_no',
            'pfr_2_no',
            'batch_start_datetime',
            'batch_end_datetime',
            'incomplete',
            'reason_of_incompletion')
            ->orderBy($sort, $order)
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($job_list as $job) {
            $job->batch_start_datetime = Carbon::parse($job->batch_start_datetime)->toDayDateTimeString();
            $job->batch_end_datetime = Carbon::parse($job->batch_end_datetime)->toDayDateTimeString();
        }

        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $job_list,
        ];

        echo json_encode($data, JSON_PRETTY_PRINT);
    }
}
