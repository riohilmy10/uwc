<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeShift;
use App\Services\EmployeeService;

class EmployeeAnalyticsApiController extends Controller
{
    private $employee_service;

    public function __construct()
    {
        $this->employee_service = new EmployeeService;
    }

    public function datatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        $sort = config('staticdata.employee_analytics.table-header.' . $request['order'][0]['column']);
        $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';

        $search = $request->filter ?? false;

        $total = Employee::count();
        $employee_list = Employee::select(
            'employee_id',
            'name')
            ->orderBy($sort, $order)
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($employee_list as $employee) {
            $employee->total_working_hours = $this->employee_service->formatInHours($this->employee_service->getTotalWorkingHours($employee->employee_id));
            $employee->total_inactive_hours = $this->employee_service->formatInHours($this->employee_service->getTotalInactiveHours($employee->employee_id));
            $employee->total_productive_hours = $this->employee_service->formatInHours($this->employee_service->getTotalProductiveHours($employee->employee_id));
        }

        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $employee_list,
        ];

        echo json_encode($data, JSON_PRETTY_PRINT);
    }
}
