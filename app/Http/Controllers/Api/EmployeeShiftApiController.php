<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmployeeShift;
use App\Models\Employee;
use Carbon\Carbon;

class EmployeeShiftApiController extends Controller
{
    public function create(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $employee = Employee::where('employee_id', $data['employee_id'])->first();

        if (!$employee) {
            return response()->json(['message' => 'Employee with the given employee ID not found.'], 404);
        }

        try {
            EmployeeShift::create(
                [
                    'employee_id'        => $data['employee_id'],
                    'pfr_no'             => $data['pfr_no'],
                    'clock_in_datetime'  => $data['clock_in_datetime'],
                    'production_line_no' => $data['production_line_no'],
                    'station_no'         => $data['station_no'],
                ]
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['message' => $e->getMessage()], 422);
        }

        return response()->json(
            [
                'message' => 'Employee shift record has been created.',
                'attributes' => [
                    'name' => $employee->name
                ]
            ],
            200
        );
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent(), true);;

        $employee_shift = EmployeeShift::where('employee_id', $data['employee_id'])
            ->where('pfr_no', $data['pfr_no'])
            ->where('clock_out_datetime', null)
            ->first();
            
        if (!$employee_shift) {
            return response()->json(['message' => 'Clock in record of the given employee for the day not found, cannot update clock out.'], 404);
        }

        try {
            $start = Carbon::parse($employee_shift->clock_in_datetime);
            $end = Carbon::parse($data['clock_out_datetime']);
            $working_hours = $end->diffInMinutes($start);

            $employee_shift->update(
                [
                    'clock_out_datetime' => $data['clock_out_datetime'],
                    'working_hours'      => $working_hours,
                    'inactive_hours'     => $data['inactive_hours'],
                ]
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['message' => $e->getMessage()], 422);
        }

        return response()->json(['message' => 'Employee shift record has been updated.'], 200);
    }

    public function datatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $production_line = $request->get('production_line_no');

        if ($draw == 1) {
            $sort = 'clock_in_datetime';
            $order = 'desc';
        } else {
            $sort = config('staticdata.employee_shifts.table-header.' . $request['order'][0]['column']);
            $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';
        }

        $search = $request->filter ?? false;

        $total = EmployeeShift::where('production_line_no', $production_line)
            ->where('clock_out_datetime', '!=', 'NULL')
            ->count();

        $shift_list = EmployeeShift::select(
            'employee_id',
            'pfr_no',
            'station_no',
            'clock_in_datetime',
            'clock_out_datetime',
            'working_hours',
            'inactive_hours')
            ->where('production_line_no', $production_line)
            ->where('clock_out_datetime', '!=', 'NULL')
            ->orderBy($sort, $order)
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($shift_list as $shift) {
            $shift->clock_in_datetime = Carbon::parse($shift->clock_in_datetime)->toDayDateTimeString();
            $shift->clock_out_datetime = Carbon::parse($shift->clock_out_datetime)->toDayDateTimeString();
            $shift->working_hours = number_format($shift->working_hours / 60, 2, '.', ',');
            $shift->inactive_hours = number_format($shift->inactive_hours / 60, 2, '.', ',');
        }

        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $shift_list,
        ];

        echo json_encode($data, JSON_PRETTY_PRINT);
    }
}
