<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AnalyticsService;

class ChartApiController extends Controller
{
    private $analytics_service;

    public function __construct()
    {
        $this->analytics_service = new AnalyticsService;
    }

    public function getDailyPercentageChartData(Request $request)
    {
        $data = $this->analytics_service->getDailyPercentage($request->production_line);
        
        return response()->json(
            [
                'message' => 'success',
                'attributes' => [
                    'working_hours' => $data['working_hours'],
                    'productive_hours' => $data['productive_hours'],
                    'batch_completed' => $data['batch_completed'],
                    'defects' => $data['defects'],
                    ]
                ],
                200
            );
    }

    public function getBatchCompletedChartData(Request $request)
    {
        $data = $this->analytics_service->getBatchCompleted($request->production_line ?? null);

        return response()->json(
            [
                'message' => 'success',
                'attributes' => [
                    'year' => $data['year'],
                    'station1' => $data['station1_ordered'],
                    'station2' => $data['station2_ordered'],
                    'station3' => $data['station3_ordered'],
                    ]
                ],
                200
            );
    }

    public function getDefectiveUnitsChartData(Request $request)
    {
        $data = $this->analytics_service->getDefectiveUnits($request->production_line ?? null);

        return response()->json(
            [
                'message' => 'success',
                'attributes' => [
                    'year' => $data['year'],
                    'defects' => $data['defects_ordered'],
                    ]
                ],
                200
            );
    }
}