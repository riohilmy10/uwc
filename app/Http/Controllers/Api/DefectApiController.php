<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Defect;
use Carbon\Carbon;

class DefectApiController extends Controller
{
    public function create(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        try {
            Defect::create(
                [
                    'employee_id'        => $data['employee_id'],
                    'production_line_no' => $data['production_line_no'],
                    'station_no'         => $data['station_no'],
                    'object_type'        => $data['object_type'],
                    'traceability_no'    => $data['traceability_no'],
                    'pfr_no'             => $data['pfr_no'],
                    'report_datetime'    => $data['report_datetime'],
                    'reason'             => $data['reason'],
                ]
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['message' => $e->getMessage()], 422);
        }

        return response()->json(['message' => 'Defect record has been created.'], 200);
    }

    public function datatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        if ($draw == 1) {
            $sort = 'report_datetime';
            $order = 'desc';
        } else {
            $sort = config('staticdata.defects.table-header.' . $request['order'][0]['column']);
            $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';
        }

        $search = $request->filter ?? false;

        $total = Defect::count();
        $defect_list = Defect::select(
            'pfr_no',
            'production_line_no',
            'station_no',
            'object_type',
            'traceability_no',
            'report_datetime',
            'employee_id',
            'reason')
            ->orderBy($sort, $order)
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($defect_list as $defect) {
            $defect->report_datetime = Carbon::parse($defect->report_datetime)->toDayDateTimeString();
            $defect->object_type = ucfirst($defect->object_type);
        }

        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $defect_list,
        ];

        echo json_encode($data, JSON_PRETTY_PRINT);
    }
}
