<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AnalyticsService;

class ProductionLineAnalyticsController extends Controller
{
    private $analytics_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->analytics_service = new AnalyticsService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        session(['active_nav' => 'production-line-analytics']);
        
        return view('layouts.production-line-analytics.list');
    }
}
