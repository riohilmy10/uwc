<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmployeeShiftController extends Controller
{
    public function list()
    {
        session(['active_nav' => 'employee-shifts']);

        return view('layouts.employee-shifts.list');
    }
}
