<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmployeeAnalyticsController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        session(['active_nav' => 'employee-analytics']);

        return view('layouts.employee-analytics.list');
    }
}
