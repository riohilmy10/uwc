<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Defect extends Model
{
    public $timestamps = false;

    public $table = 'defects';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id',
        'production_line_no',
        'station_no',
        'object_type',
        'traceability_no',
        'pfr_no',
        'report_datetime',
        'reason'
    ];
}
