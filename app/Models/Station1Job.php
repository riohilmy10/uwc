<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Station1Job extends Model
{
    public $timestamps = false;

    public $table = 'station_1_jobs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id',
        'production_line_no',
        'body_traceability_no',
        'pfr_1_no',
        'batch_start_datetime',
        'batch_end_datetime',
        'incomplete',
        'reason_of_incompletion'
    ];
}
