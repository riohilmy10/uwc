<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeShift extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id',
        'pfr_no',
        'clock_in_datetime',
        'production_line_no',
        'station_no',
        'clock_out_datetime',
        'working_hours',
        'inactive_hours',
    ];
}
