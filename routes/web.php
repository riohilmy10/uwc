<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'employees'], function () {
        Route::get('/', 'EmployeeController@list')->name('employees.view.list');
        Route::get('create', 'EmployeeController@createView')->name('employees.view.create');
        Route::get('/{id}', 'EmployeeController@edit')->name('employees.view.edit');
        Route::post('/', 'EmployeeController@create')->name('employees.create');
        Route::put('/{id}', 'EmployeeController@update')->name('employees.update');
    });
    Route::group(['prefix' => 'defects'], function () {
        Route::get('/', 'DefectController@list')->name('defects.view.list');
        Route::get('create', 'DefectController@createView')->name('defects.view.create');
        Route::post('/', 'DefectController@createView')->name('defects.create');
    });
    Route::group(['prefix' => 'traceabilities'], function () {
        Route::get('/', 'StationController@list')->name('stations.view.list');
    });
    Route::group(['prefix' => 'employee-analytics'], function () {
        Route::get('/', 'EmployeeAnalyticsController@list')->name('employee-analytics.view.list');
    });
    Route::group(['prefix' => 'production-line-analytics'], function () {
        Route::get('/', 'ProductionLineAnalyticsController@index')->name('production-line-analytics.view.index');
    });
    Route::group(['prefix' => 'employee-shifts'], function () {
        Route::get('/', 'EmployeeShiftController@list')->name('employee-shifts.view.list');
    });
});