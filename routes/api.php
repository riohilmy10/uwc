<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {
    Route::group(['prefix' => 'employee-shifts'], function () {
        Route::post('/', 'EmployeeShiftApiController@create')->name('employee-shifts.create');
        Route::post('/update', 'EmployeeShiftApiController@update')->name('employee-shifts.update');
        Route::get('/datatable', 'EmployeeShiftApiController@datatableList')->name('employee-shifts.datatable.list');
    });
    Route::group(['prefix' => 'station-jobs'], function () {
        Route::post('/1', 'Station1JobApiController@create')->name('station-1-jobs.create');
        Route::post('/2', 'Station2JobApiController@create')->name('station-2-jobs.create');
        Route::post('/3', 'Station3JobApiController@create')->name('station-3-jobs.create');
        Route::get('/datatable/1', 'Station1JobApiController@datatableList')->name('station-1-jobs.datatable.list');
        Route::get('/datatable/2', 'Station2JobApiController@datatableList')->name('station-2-jobs.datatable.list');
        Route::get('/datatable/3', 'Station3JobApiController@datatableList')->name('station-3-jobs.datatable.list');
    });
    Route::group(['prefix' => 'defects'], function () {
        Route::post('/', 'DefectApiController@create')->name('defects.create');
        Route::get('/datatable', 'DefectApiController@datatableList')->name('defects.datatable.list');
    });
    Route::group(['prefix' => 'charts'], function () {
        Route::get('/daily-percentage', 'ChartApiController@getDailyPercentageChartData')->name('charts.daily-percentage');
        Route::get('/batch-completed', 'ChartApiController@getBatchCompletedChartData')->name('charts.batch-completed');
        Route::get('/defect', 'ChartApiController@getDefectiveUnitsChartData')->name('charts.defects');
    });
    Route::group(['prefix' => 'employee-analytics'], function () {
        Route::get('/datatable', 'EmployeeAnalyticsApiController@datatableList')->name('employee-analytics.datatable.list');
    });
});
